From python:3.6.4-slim-jessie

RUN pip install cherrypy
RUN pip install requests

WORKDIR /ws
ADD ws.py .
ADD check_url.py .
RUN mkdir logs

# Start web service
ENTRYPOINT ["python", "/ws/ws.py"]
CMD ["--logLevel", "INFO"]
