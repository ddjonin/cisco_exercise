import requests

TEST_LEN = 10

def test_malware():
    r = requests.get("http://localhost:8080/urlinfo/1/malinator.com/")

    assert(r.status_code == 200)
    #print("Content: {}".format(r.content))
    assert(r.content == b'Maleware')

def test_safe_site():
    r = requests.get("http://localhost:8080/urlinfo/1/www.google.com/")
    #print("Content: {}".format(r.content))
    assert(r.status_code == 404)

if __name__ == '__main__':

    for i in range(TEST_LEN):
        test_malware()
        test_safe_site()