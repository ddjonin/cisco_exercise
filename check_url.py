import requests
from functools import lru_cache
import cherrypy

# Apility Config
API_KEY = 'c4e98625-5b89-49c6-b37e-77217a8df02d'
APILITY_URL = 'http://api.apility.net/baddomain/{}'
HEADERS = {'X-Auth-Token': '{}'.format(API_KEY)}

LRU_CACHE_SIZE = 100

@lru_cache(maxsize=LRU_CACHE_SIZE)
def check_url(url_to_query):

    # Query the Apility URL database
    r = requests.get(APILITY_URL.format(url_to_query), headers=HEADERS)
    cherrypy.log('Non-cached URL to Query: %s. ApilityURL: %s. Response: %s, Status Codess: %s' % (url_to_query, APILITY_URL.format(url_to_query), r.content, r.status_code))

    # Form the HTTP response
    res = form_response(r)
    return res

def form_response(r):

    if (r.status_code == 200):
        res = 'Maleware'
    else:
        res = 'Good'

    return res
