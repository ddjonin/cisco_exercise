URL Checking Web Service
-------------
This is a RESTful web service that provides API for the scanner for URL maleware status analysis. Web service is implemented using Python. Web service is implemented to be scalable and simple to use.
   
This example web service comes with some core features considered important for hosting web service in the public cloud:

- *Logging* - **File and console logging** for diagnostic purposes, supports different logging level
- *Deployment* - Simple build and run scripts for setting up the web service to run inside **Docker**
- *JSON request* - Processing **JSON data in HTTP request/response** with CherryPy
- *Caching* - Application maintains an LRU (Least Recent Used) cache of programmable size to maintain database of URL health request checks.

After checking out this project, you will have a web service running in Docker.

#### Requirements
- Linux (e.g. Ubuntu 16.04)
- Docker CE or EE installed

## API How-To

Once the web service has started, it can be queried using the following API on the local host:

http://localhost:8080/urlinfo/1/url_to_query/

The web service will return:

- **200 OK**: if url_to_query is in the database of URL-s that are considered malware 
- **404 Not found**: if url_to_query is **not** in the database of URL-s that are considered malware 

### Testing:

One can run a simple unit test to verify that web service is working correctly:

```
python test_python_ws.py
```

## Dockerfile explained
``` 
From python:3.6.4-slim-jessie
```

``` 
RUN pip install cherrypy==11.0
```
- The above commands install the required Python packages.  Cherrypy is the web framework for serving the web service.
<br />

``` 
WORKDIR /ws
ADD ws.py  .
ADD check_url.py .
RUN mkdir logs
```
- Next, a working directory called *ws* is created for storing the python script, data files and log files
<br />

```
ENTRYPOINT ["python", "/ws/ws.py"]
CMD ["--logLevel", "INFO"]
``` 
- Finally, the ENTRYPOINT defines the command to run when the container starts up - i.e. python /ws/ws.py.  The subsequent CMD command defines the default arguments to be passed into ws.py, e.g. set log level to INFO. 

## Installation: 
### Build Docker image
- Clone this project

- Navigate to project directory and build the docker image with a tag:
```
docker build -t python-ws .
```

### Start web service
- Run docker container:
```  
docker run -p 8080:8080 python-ws:latest
```
If the web service starts successfully, these logging statements should appear in the console:
```
[10/Oct/2017:10:33:29] ENGINE Listening for SIGTERM.
[10/Oct/2017:10:33:29] ENGINE Listening for SIGHUP.
[10/Oct/2017:10:33:29] ENGINE Listening for SIGUSR1.
[10/Oct/2017:10:33:29] ENGINE Bus STARTING
[10/Oct/2017:10:33:29] ENGINE Started monitor thread 'Autoreloader'.
[10/Oct/2017:10:33:29] ENGINE Started monitor thread '_TimeoutMonitor'.
[10/Oct/2017:10:33:29] ENGINE Serving on http://0.0.0.0:8080
[10/Oct/2017:10:33:29] ENGINE Bus STARTED
```

### Run options

#### Run in detached mode
Add -d flag:
```
docker run -d python-ws:latest
```

## Questions from the CISCO exercise

1) The size of the URL list could grow infinitely, how might you scale this beyond the memory capacity of this
VM?

* Current implementation of the URL analysis web service is implemented using a containerized application which stores all URL queries in the 
database. This database is based on the LRU cache of programmable size which can be adjusted to fit the memory of the host device. If a request that needs to be served is not in the LRU cache, the service will query the third party Apility API to get the information on the URL status. That information will be pushed in the LRU cache. If the cache size is exhausted, the least recently used entry will be deleted from the cache. Scaling of the application as described in the next question will also increase the memory capacity as the cache would be distributed across several hosts.

An alternative approach, if one needs to store many URL queries and not rely regularly on the third-party site, is to use a localized or distributed database to store URL information. One can for example use MongoDb to store either locally or in a distributed fashion information about URL-s.

2) The number of requests may exceed the capacity of this VM, how might you solve that? 

* This implementation of the URL analysis web service is containerized, hence it can be can be easily replicated across several hosts. If number of requests into one host exceeds its performance, this service can be replicated across several hosts and  a load balancer such as NGINX can be introduced to load balance the requests across a farm of URL analysis web services.

3) What are some strategies you might use to update the service with new URLs? Updates may be as much as 5
thousand URLs a day with updates arriving every 10 minutes.

* To update the web service with new URL information, one can execute periodically as a seaprate thread batch-update of the local database with list of new URL-s. In Python this can be implemented using a threading library that will schedule execution of these tasks. A simple way  how to accomplish this would be:

```
UPDATE_PERIOD = 15*60
import time, threading
def cache_batch_update():
    print(time.ctime())
    threading.Timer(UPDATE_PERIOD, cache_batch_update).start()
```

*which will perform cache_batch_update roughly every 15 minutes.*

