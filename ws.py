import argparse
import cherrypy
import logging.config
import requests
from check_url import check_url

#LOCAL_DIR = '/ws/'
LOCAL_DIR = ''
LOG_DIR = LOCAL_DIR + 'logs/'
PORT = 8080

parser = argparse.ArgumentParser()
parser.add_argument('--logLevel', default='INFO', help='Update log level, default level is INFO')
parser.add_argument('--ssl', action='store_true', help='Enable SSL', default=False)
args = parser.parse_args()
LOG_LEVEL = args.logLevel

# Log Configuration for CherryPy Server log only, storing cherrypy logs in LOG_DIR
logging.config.dictConfig(
    {
    'version' : 1,

    'formatters': {
        'void': {
            'format': ''
        },
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
    },
    'handlers': {
        'default': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_console': {
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'void',
            'stream': 'ext://sys.stdout'
        },
        'cherrypy_access': {
            'level':'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'void',
            'filename': LOG_DIR + 'cp_access.log',
            'maxBytes': 10485760,
            'backupCount': 20,
            'encoding': 'utf8'
        },
        'cherrypy_error': {
            'level':'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'formatter': 'void',
            'filename': LOG_DIR + 'cp_errors.log',
            'maxBytes': 10485760,
            'backupCount': 20,
            'encoding': 'utf8'
        },
    },
    'loggers': {
        '': {
            'handlers': ['default'],
            'level': LOG_LEVEL
        },
        'db': {
            'handlers': ['default'],
            'level': LOG_LEVEL,
            'propagate': False
        },
        'cherrypy.access': {
            'handlers': ['cherrypy_access'],
            'level': LOG_LEVEL,
            'propagate': False
        },
        'cherrypy.error': {
            'handlers': ['cherrypy_console', 'cherrypy_error'],
            'level': LOG_LEVEL,
            'propagate': False
        },
    }
})

class UrlInfoHandler(object):
    def __init__(self):
        self.urlinfohandler = UrlQuery()

    def _cp_dispatch(self, vpath):
        if (len(vpath) != 3):
            return self

        if len(vpath) == 3:
            target = vpath.pop(0)           # /urlinfo target/
            if target != 'urlinfo':
                return self

            cherrypy.request.params['version'] = vpath.pop(0)           # / API version/
            cherrypy.request.params['url_to_query'] = vpath.pop(0)      # / url_to_query/
            return self.urlinfohandler

        return vpath

    @cherrypy.expose
    def index(self):
        return 'Wrong format. UrlInfo API Reached. Please use this format: /urlinfo/1/original_path_and_query_string'

class UrlQuery(object):
    @cherrypy.expose
    def index(self, version, url_to_query):
        if version == '1':
            response = check_url(url_to_query)
            cherrypy.log("LRU Cache size: {}".format(check_url.cache_info()))
            if response == "Good":
                raise cherrypy.HTTPError(404)

            return response
        else:
            return 'Wrong API version %s' % (version)

if __name__ == '__main__':

    # Global config
    cherrypy.config.update({'server.socket_port': PORT,
                            'server.socket_host': '0.0.0.0'})

    # Web Service config
    conf = {
        '/': {
            'response.timeout': 6000,
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.sessions.on': True,
            'tools.response_headers.on': True,
            'tools.response_headers.headers': [('Content-Type', 'text/plain')]
        }
    }

    cherrypy.quickstart(UrlInfoHandler())
